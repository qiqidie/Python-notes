# Python-notes

## 介绍
cungudafa的python学习笔记

## 博客链接

>[cungudafa.blog.csdn.net](https://cungudafa.blog.csdn.net/)

## 食用注意

`注意相关路径！`

## 具体内容

|                                文件夹                                | 内容                                                         | 相关博客                                                     |
| :-----------------------------------------------------------------: | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [beyes](https://gitee.com/cungudafa/Python-notes/tree/master/beyes) | <img src="https://img-blog.csdnimg.cn/20200316005908926.png" style="zoom: 33%;" /> | [【Sklearn】入门花卉数据集实验--理解朴素贝叶斯分类器](https://cungudafa.blog.csdn.net/article/details/104890498) |
| [CNN](https://gitee.com/cungudafa/Python-notes/tree/master/CNN) | <img src="https://img-blog.csdnimg.cn/20200320143153344.png" style="zoom:50%;" /> | [【TensorFlow2&Keras】训练手语图像数据集--基于卷积神经网络CNN](https://blog.csdn.net/cungudafa/article/details/104954336) |
| [yolo3](https://gitee.com/cungudafa/Python-notes/tree/master/yolov3) | <img src="https://img-blog.csdnimg.cn/20200322153609835.png" style="zoom: 33%;" /> | [【Yolo3】入门目标检测实验--Python+Opencv2+dnn](https://blog.csdn.net/cungudafa/article/details/105028034) |



如有帮助，请给我一个star哦！

如果帮助颇深，可以点击下方**捐赠**，给小王点一杯奶茶哦！小王会动力满满输出代码的。



#### 作者

cungudafa
